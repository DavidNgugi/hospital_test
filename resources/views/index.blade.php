﻿@extends('layouts.layout')
@section('content')
   
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <h4 class="page-head-line">Dashboard</h4>
              </div>

          </div>
       
          <div class="row">

              <a href = "/users"><div class="col-md-4 col-sm-4 col-xs-6">
                  <div class="dashboard-div-wrapper bk-clr-one">
                      <i  class="fa fa-users dashboard-div-icon" ></i>
                       <h5>Users</h5>
                  </div>
              </div></a>

               <a href = "/patients"><div class="col-md-4 col-sm-4 col-xs-6">
                  <div class="dashboard-div-wrapper bk-clr-two">
                      <i  class="fa fa-bed dashboard-div-icon" ></i>
                      
                       <h5>Patients</h5>
                  </div>
              </div></a>
               

               <a href = "/payments"><div class="col-md-4 col-sm-4 col-xs-6">
                  <div class="dashboard-div-wrapper bk-clr-four">
                      <i  class="fa fa-money dashboard-div-icon" ></i>
                     
                       <h5>Payments</h5>
                  </div>
              </div></a>

          </div>

         {{--  <div class="row">
             <div class="col-md-12">
                  <h4 class="page-head-line">Actions</h4>
              </div>
          </div> --}}
         
         
      </div>

    @endsection

  