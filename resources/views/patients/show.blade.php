@extends('layouts.layout')
@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Create New Patient</h4>
            </div>
        </div>
     
         <div class="row col-md-12">
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
             <table class="table table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
               <tr>
                  <td colspan="2"><h3>Personal Info</h3></td>
                </tr>
                <tr>
                  <td>Full Name</td><td>{{ $patient->name }}</td>
                </tr>
                 <tr>
                  <td>D.O.B</td><td>{{ $patient->dob }}</td>
                </tr>
                 <tr>
                  <td>Insurance COver</td><td>{{ $patient->insurance_cover }}</td>
                </tr>
                <tr>
                  <td>Policy Number</td><td>{{ $patient->policy_number }}</td>
                </tr>
                <tr>
                  <td>Telephone</td><td>{{ json_decode($patient->contacts)->tel }}</td>
                </tr>
                <tr>
                  <td>Email Address</td><td>{{ json_decode($patient->contacts)->email }}</td>
                </tr>
              </tbody>
            </table>          
           </div>

           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
             <table class="table table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="2"><h3>Next of Kin</h3></td>
                </tr>
                <tr>
                  <td>Full Name</td><td>{{ $patient->nextOfKin->name }}</td>
                </tr>
                <tr>
                  <td>Telephone</td><td>{{ json_decode($patient->nextOfKin->contacts)->tel }}</td>
                </tr>
                <tr>
                  <td>Email Address</td><td>{{ json_decode($patient->nextOfKin->contacts)->email }}</td>
                </tr>
              </tbody>
            </table>          
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a class="btn btn-primary" href = "{{ url('/patients/'.$patient->id. '/edit') }}"><i class="fa fa-edit"></i> Edit</a>
            <a class="btn btn-danger" href = "{{ url('/patients/'.$patient->id. '/delete') }}"><i class="fa fa-trash"></i> Delete</a>
          </div>
          
    
        </div>
       
       
    </div>
    

@endsection
    
  