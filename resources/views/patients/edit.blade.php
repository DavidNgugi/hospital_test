@extends('layouts.layout')
@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Edit Patient: {{ $patient->name }}</h4>
            </div>
        </div>
     
         <div class="row">
      		<form action="{{ url('/patients'.$patient->id.'/edit') }}" method="POST" role="form">
      			{{ csrf_field() }}
      			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      				
      			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      				
      			<div class="form-group">
      				<label for="">Full Name</label>
      				<input type="text" class="form-control" id="" name="name" value="{{ $patient->name }}" placeholder="Full Name">
      			</div>
      			
      			<div class="form-group">
      				<label for="">Date of Birth</label>
      				<input type="date" class="form-control" id="" name="dob" value="{{ $patient->dob }}" placeholder="Date of Birth">
      			</div>

      			<div class="form-group">
      				<label for="">Insurance Cover</label>
      				<select name="insurance_cover" id="insurance_cover" class="form-control" required="required">
      					<option value="jubilee">Jubilee Insurance</option>
      				</select>
      			</div>

      			<div class="form-group">
      				<label for="">Policy Number</label>
      				<input type="text" class="form-control" id="" name="policy_number" value="{{ $patient->policy_number }}" placeholder="Policy Number">
      			</div>

      			<div class="form-group">
      				<label for="">Telphone</label>
      				<input type="number" class="form-control" id="" name="tel" value="{{ json_decode($patient->contacts)->tel }}" placeholder="Telephone">
      			</div>

      			<div class="form-group">
      				<label for="">Email Adress</label>
      				<input type="email" class="form-control" id="" name="email" value="{{ json_decode($patient->contacts)->email }}" placeholder="Email Address">
      			</div>
      			</div>

      			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	      			<h3>Next of Kin</h3>
	      				<div class="form-group">
	      				<label for="">Full Name</label>
	      				<input type="text" class="form-control" id="" name="next_of_kin_name" value = "{{ $patient->nextOfKin->name }}"" placeholder="Full Name">
	      			</div>

	      			<div class="form-group">
	      				<label for="">Telphone</label>
	      				<input type="number" class="form-control" id="" name="next_of_kin_tel" value = "{{ json_decode($patient->nextOfKin->contacts)->tel }}" placeholder="Telephone">
	      			</div>

	      			<div class="form-group">
	      				<label for="">Email Adress</label>
	      				<input type="email" class="form-control" id="" name="next_of_kin_email" value = "{{ json_decode($patient->nextOfKin->contacts)->email }}" placeholder="Email Address">
	      			</div>
      			</div>
      			
      			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">      			
      				<button type="submit" class="btn btn-primary">Submit</button>
      			</div>
      			</div>
      		</form>

        </div>
       
       
    </div>
    

@endsection
    
  