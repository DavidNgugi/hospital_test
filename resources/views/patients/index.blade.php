@extends('layouts.layout')
@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">All Patients</h4>
                 <a href = "/patients/create" class="btn btn-default">Add a Patient</a>
            </div>

        </div>
     
          <div class="row" style="margin-top: 10px;">
          @if(count($patients) > 0)
        		<table class="table table-hover datatable">
        			<thead>
        				<tr>
        					<th>Action</th>
        					<th>Name</th>
        					<th>Tel</th>
        					<th>Email</th>
        					<th>Checked In</th>
        				</tr>
        			</thead>
        			<tbody>
        				@foreach($patients as $patient)
  	      				<tr>
  	      					<td><a href = "{{ url('/patients/'.$patient->id. '/edit') }}">Edit</a></td>
  	      					<td><a href = "{{ url('/patients/'.$patient->id) }}">{{ ucwords($patient->name) }}</a></td>
  	      					<td>{{ json_decode($patient->contacts)->tel }}</td>
  	      					<td>{{ json_decode($patient->contacts)->email }}</td>

                      <td>@if($patient->checked_in == "true") <span class="badge badge-success">Checked In</span> @else <span class="badge badge-danger">Checked Out</span>@endif</td>
                      <td>

                      @if($patient->checked_in == "true")
                        <a class="btn btn-primary" data-toggle="modal" id = "btnCheckOut" href="{{ url('/checkout/'.$patient->id) }}">                               
                            Check Out
                        </a>
                      @else
                        <a class="btn btn-primary" data-toggle="modal" id="btnCheckIn" href="{{ url('/checkin/'.$patient->id) }}">
                            Check In
                        </a>
                      @endif
                      </td>
  	      				</tr>
        				@endforeach
        			</tbody>
        		</table>
      		@else
      			<div class="alert alert-info">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<strong>Sorry!</strong> There are no patient records
      			</div>
      		@endif
        </div>
       
       
       
    </div>
    

   

@endsection
    
  