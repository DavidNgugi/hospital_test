@extends('layouts.layout')
@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">All Users</h4>
                <a href = "/users/create" class="btn btn-default">Add a User</a>
            </div>

        </div>
     
        <div class="row" style="margin-top: 10px;">
        @if(count($users) > 0)
      		<table class="table table-hover datatable">
      			<thead>
      				<tr>
      					<th>Action</th>
      					<th>Name</th>
      					<th>Tel</th>
      					<th>Email</th>
      					<th>Access level</th>
      				</tr>
      			</thead>
      			<tbody>
      				@foreach($users as $user)
	      				<tr>
	      					<td><a href = "{{ url('/users/'.$user->id. '/edit') }}">Edit</a></td>
	      					<td><a href = "{{ url('/users/'.$user->id) }}">{{ ucwords($user->name) }}</a></td>
	      					<td>{{ $user->tel }}</td>
	      					<td>{{ $user->email }}</td>
	      					<td>{{ $user->acl }}</td>
	      				</tr>
      				@endforeach
      			</tbody>
      		</table>
          @else
            <div class="alert alert-info">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Sorry!</strong> There are no user records
            </div>
          @endif
        </div>
       
       
    </div>
    

@endsection
    
  