@extends('layouts.layout')
@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Create New User</h4>
            </div>
        </div>
     
        <div class="row col-md-6">
      		<form action="{{ url('/users') }}" method="POST" role="form">
      		  {{ csrf_field() }}
      			<div class="form-group">
      				<label for="">Full Name</label>
      				<input type="text" class="form-control" id="" name="name" placeholder="Full Name">
      			</div>
      			
      			<div class="form-group">
      				<label for="">Telphone</label>
      				<input type="number" class="form-control" id="" name="tel" placeholder="Telephone">
      			</div>

      			<div class="form-group">
      				<label for="">Email Adress</label>
      				<input type="email" class="form-control" id="" name="email" placeholder="Email Address">
      			</div>

      			<div class="form-group">
      				<label for="">Access level</label>
      				<select name="acl" id="input" class="form-control" required="required">
      					<option value="admin">Admin</option>
      					<option value="doctor">Doctor</option>
      					<option value="secretary" selected>Secretary</option>
      				</select>
      			</div>
      		
      			<button type="submit" class="btn btn-primary">Submit</button>
      		</form>

        </div>
       
       
    </div>

@endsection
    
  