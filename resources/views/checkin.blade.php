@extends('layouts.layout')
@section('content')
   
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <h4 class="page-head-line">Check In Patient</h4>
          </div>
      </div>
   
      <div class="row col-md-6">
        <form action="{{ route('checkIn') }}" method="POST">
            {{ csrf_field() }}

            <input type="hidden" name="patient_id" value="{{ $patient->id }}" required>

              <div class="form-group">
                <label for="">Patient Name</label>
                <input type="text" class="form-control" disabled name="name" value="{{ $patient->name }}" required>
              </div>

              <div class="form-group">
                <label for="">Payment Mode</label>
                <select name="payment_mode" id="inputPayment_mode" onchange="showAmount(this)" class="form-control" required>
                  <option value="cash">Cash</option>
                  <option value="cheque">Cheque</option>
                  <option value="insurance">Insurance</option>
                  <option value="credit">Credit</option>
                </select>
              </div>

              <div class="form-group">
                <label for="">Amount</label>
                <input type="number" name="amount" id = "amount" disabled class="form-control">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
        </form>
            
      </div>
     
  </div>
  
@endsection

@push('scripts')
  <script type="text/javascript">
    function showAmount(elem){
      if(elem.value == "credit" ){
        $("#amount").attr("disabled", false);
      }
      else{
         $("#amount").value = 0;
         $("#amount").attr("disabled", true);
      }
    }
  </script>

@endpush
