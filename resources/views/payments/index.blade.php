@extends('layouts.layout')
@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">All Payments</h4>
            </div>

        </div>

        <div class="row" style="margin-top: 10px;">
          @if(count($payments) > 0)
        		<table class="table table-hover datatable">
        			<thead>
        				<tr>
                  <th>ID</th>
        					<th>Patient Name</th>
        					<th>Mode</th>
        					<th>Amount</th>
        					<th>Status</th>
        				</tr>
        			</thead>
        			<tbody>
        				@foreach($payments as $payment)
          				<tr>
                    <td>#{{ $payment->id }}</td>
                    <td><a href = "{{ url('/patient/'.$payment->patient_id) }}">{{ ucwords($payment->patient->name) }}</a></td>
          					<td>{{ $payment->mode }}</td>
          					<td>KES {{ $payment->amount }}</td>
                    <td>
                      @if($payment->status == "paid") 
                        <span class="label label-success">Paid</span> 
                      @elseif($payment->status == "rejected") 
                        <span class="label label-danger">Rejected</span> 
                      @elseif($payment->status == "pending" and Carbon\Carbon::parse($payment->created_at)->diffInDays(Carbon\Carbon::now()) < 7) 
                        <span class="label label-info">Pending</span> 
                      @elseif($payment->status == "overdue" and \Carbon::parse($payment->created_at)->diffInDays(\Carbon::now()) > 7)
                        <span class="label label-warningr">Overdue</span>
                      @endif
                    </td>
                      
          				</tr>
        				@endforeach
        			</tbody>
        		</table>
        	@else
        		<div class="alert alert-info">
        			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        			<strong>Sorry!</strong> There are no payment records
        		</div>
        	@endif
        </div>

    </div>

@endsection