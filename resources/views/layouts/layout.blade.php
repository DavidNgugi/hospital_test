<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Hospital dev</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />

     <!-- DataTable Styles -->
    <link href="{{ asset('assets/js/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <strong>Email: </strong>info@yourdomain.com
                    &nbsp;&nbsp;
                    <strong>Support: </strong>+90-897-678-44
                </div>

            </div>
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <h2>Hospital MIS</h2>
                </a>

            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a @if(Request::segment(1) == 'home' or Request::segment(1) == '') class="menu-top-active" @endif href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a @if(Request::segment(1) == 'users') class="menu-top-active" @endif href="{{ url('/users') }}">Users</a></li>
                            <li><a @if(Request::segment(1) == 'patients') class="menu-top-active" @endif href="{{ url('/patients') }}">Patients</a></li>
                            <li><a href="{{ url('/payments') }}">Payments</a></li>
                            
                            @if(Auth::check())
                                <li> <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                            @else
                                <li><a href="/login">Login</a></li>
                            @endif
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        @yield('content')
    </div>

  <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; {{ date('Y') }} Collabmed
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="{{ asset('assets/js/jquery-1.11.1.js') }}"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="{{ asset('assets/js/bootstrap.js') }}"></script>

    <script src="{{ asset('assets/js/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('assets/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        if($('.datatable') !== undefined || $('.datatable') !== null){
            $('.datatable').dataTable();
        }
    </script>

    <script>
        @if(Session::has('flash_info'))
            $.notify({
            icon: 'fa fa-bell fa-2x',
            message: '{!! Session::get('flash_info') !!}'
        },{
            type: 'info',
            timer: 4000
        });
        @endif
        @if(Session::has('flash_success'))
            $.notify({
            icon: 'fa fa-bell fa-2x',
            message: '{!! Session::get('flash_success') !!}'
        },{
            type: 'success',
            timer: 4000
        });
        @endif
        @if(Session::has('flash_warning'))
            $.notify({
            icon: 'fa fa-bell fa-2x',
            message: '{!! Session::get('flash_warning') !!}'
        },{
            type: 'warning',
            timer: 4000
        });
        @endif
        @if(Session::has('flash_error'))
            $.notify({
            icon: 'fa fa-bell fa-2x',
            message: '{!! Session::get('flash_error') !!}'
        },{
            type: 'danger',
            timer: 4000
        });
        @endif
    </script>

    <script type="text/javascript">
        $('#btnCheckIn').click(function(){
            $("#modal-checkin-id").modal().show();
        });
        
        $('#btnCheckOut').click(function(){
            $("#modal-checkout-id").modal().show();
        });
    </script>

    @stack('scripts')
</body>
</html>
