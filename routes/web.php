<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'IsOnlyAdmin'], function() {
    Route::resource('users', 'UsersController');
});

Route::resource('patients', 'PatientsController');

// Checkin - Checkout
Route::get('checkin/{id}','PatientsController@checkIn');
Route::post('checkin','PatientsController@checkInPatient')->name('checkIn');
Route::get('checkout/{id}','PatientsController@checkOut');
Route::post('checkout','PatientsController@checkOutPatient')->name('checkOut');

// Payment Routes

// Route::group(['middleware' => 'IsOnlyAdmin'], function() {
	Route::resource('payments','PaymentsController');
// });