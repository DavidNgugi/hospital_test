<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Payment;

class PaymentsController extends Controller
{
	private $payment;

	public function __construct(Payment $payment){
		$this->payment = $payment;
	}

    public function index(){
    	$payments = $this->payment->latest()->get();
    	return view('payments.index', compact('payments'));
    }

    public function show($id){
    	$payment = Payment::findOrFail($id);
    	return view('payments.show', compact('payment'));
    }
}
