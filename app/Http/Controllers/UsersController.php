<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UsersController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->middleware('IsOnlyAdmin');
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->latest()->get();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->user->name = $request->name;
            $this->user->tel = $request->tel;
            $this->user->email = $request->email;
            $this->user->password = bcrypt(str_random(8));
            $this->user->acl = $request->acl;
            $this->user->save();
            return redirect('/users')->with('flash_success', 'User created!');
        }catch(\Exception $e){
            dd($e->getMessage());
            return back()->with('flash_error', 'User could not be created!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id)->first();
        if($user != null)
        {
            return view('users.show')->with(['user' => $user]);
        }else{
            return back()->with('flash_error','User not found!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id)->first();
        if($user != null)
        {
            return view('users.edit')->with(['user' => $user]);
        }else{
            return back()->with('flash_error','User not found!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $user = User::findOrFail($id)->first();
            $user->name = $request->name;
            $user->tel = $request->tel;
            $user->email = $request->email;
            $user->acl = $request->acl;
            $user->save();
            return redirect('/users')->with('flash_success', 'User created!');
        }catch(\Exception $e){
            return back()->with('flash_error', 'User could not be created!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id)->first();
        if($user != null)
        {
            User::delete($id);
            return view('users.index')->with('flash_success', 'User deleted!');
        }else{
            return back()->with('flash_error','User not found!');
        }
    }
}
