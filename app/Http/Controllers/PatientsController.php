<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Patient;
use App\NextOfKin;
use App\Operation;
use App\Payment;

class PatientsController extends Controller
{
    private $patient;
    private $nextOfKin;
    private $operation;
    private $payment;

    public function __construct(Patient $patient, NextOfKin $nextOfKin, Operation $operation, Payment $payment)
    {
        $this->patient = $patient;
        $this->nextOfKin = $nextOfKin;
        $this->operation = $operation;
        $this->payment = $payment;
    }

    public function checkIn($id)
    {
        try{
            $patient = Patient::findOrFail($id);
            return view('checkin', compact('patient'));
        }catch(\Exception $e){
            return back()->with('flash_error',"Patient couldn't be found");
        }
    }

    public function checkInPatient(Request $request)
    {
        try{
            $this->operation->patient_id = $request->patient_id;
            $this->operation->payment_mode = $request->payment_mode;
            if($request->amount > 0){
                $this->operation->amount = -$request->amount;
            }else{
                 $this->operation->amount = 0;
            }
            $this->operation->save();
            
            $patient = Patient::findOrFail($request->patient_id);
            $patient->checked_in = "true";
            $patient->save();
            return back()->with('flash_success',"Patient checked in");
        }catch(\Exception $e){
            return back()->with('flash_error',"Patient couldn't be checked in");
        }
    }

    public function checkOut($id)
    {
        try{
            $patient = Patient::findOrFail($id);
            return view('checkout', compact('patient'));
        }catch(\Exception $e){
            return back()->with('flash_error',"Patient couldn't be found");
        }
    }

    public function checkOutPatient(Request $request)
    {
        try{
           
            $operation = $this->operation->where('patient_id', $request->patient_id)->get();
            $this->payment->patient_id = $request->patient_id;
            $this->payment->mode = $request->payment_mode;

            if($request->amount > 0){
                $this->payment->amount = $request->amount;
            }else{
                 $this->payment->amount = 0;
            }
            $this->payment->save();

            // generate invoice here 

            $patient = Patient::findOrFail($request->patient_id);
            $patient->checked_in = "false";
            $patient->save();
            return back()->with('flash_success',"Patient checked out");
        }catch(\Exception $e){
            return back()->with('flash_error',"Patient couldn't be checked out");
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = $this->patient->latest()->get();
        return view('patients.index', compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->patient->name = $request->name;
            $this->patient->dob = $request->dob;
            $this->patient->contacts = json_encode(['tel' => $request->tel, 'email' => $request->email]);
            $this->patient->insurance_cover = $request->insurance_cover;
            $this->patient->policy_number = $request->policy_number;
            $this->patient->save();
            // next of kin
            $this->nextOfKin->name = $request->next_of_kin_name;
            $this->nextOfKin->patient_id =  $this->patient->id;
            $this->nextOfKin->contacts = json_encode(["tel"=>$request->next_of_kin_tel, "email" => $request->next_of_kin_email]);
            $this->nextOfKin->save();
            return back()->with('flash_success', 'Patient record created!');
        }catch(Exception $e){
            return back()->with('flash_error', 'Patient record could not be created! '. $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::findOrFail($id)->first();
        if ($patient != null) {
            return view('patients.show')->with(['patient' => $patient]);
        }else{
            return back()->with('flash_error', 'Patient record not found!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::findOrFail($id)->first();
        if ($patient != null) {
            return view('patients.edit')->with(['patient' => $patient]);
        }else{
            return back()->with('flash_error', 'Patient record not found!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patient = Patient::findOrFail($id)->first();
        if ($patient != null) {
            $patient->name = $request->name;
            $patient->dob = $request->dob;
            $patient->contacts = json_encode(['tel' => $request->tel, 'email' => $request->email]);
            $patient->insurance_cover = $request->insurance_cover;
            $patient->policy_number = $request->policy_number;
            $patient->save();

            $nextOfKin = NextOfKin::findOrFail($patient->next_of_kin_id);
            if($nextOfKin != null) 
            {
                $nextOfKin->name = $request->next_of_kin_name;
                $nextOfKin->contacts = json_encode(["tel"=>$request->next_of_kin_tel, "email" => $request->next_of_kin_email]);
                $nextOfKin->save();
            }
            return back()->with('flash_error', 'Patient record updated!');
        }else{
            return back()->with('flash_error', 'Patient record not found!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::findOrFail($id)->first();
        if ($patient != null) {
            Patient::delete($id);
            return view('patient.index')->with('flash_success', 'Patient record deleted!');
        }else{
            return back()->with('flash_error', 'Patient record not found!');
        }
    }
}
