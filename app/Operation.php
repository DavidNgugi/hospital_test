<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
	public $table = "operations";

    public function patients()
    {
    	return $this->hasMany('App\Patient');
    }
}
