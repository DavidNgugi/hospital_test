<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    public $table = "patients";

    protected $casts = [
	    'contacts' => 'json', // Will converted to (json)
	];

    public function nextOfKin(){
    	return $this->hasOne('App\NextOfKin');
    }

    public function doctor(){
    	return $this->hasOne('App\User');
    }

    public function operation(){
        return $this->hasOne('App\Operation');
    }

}
