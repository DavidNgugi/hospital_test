<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NextOfKin extends Model
{
    public $table = "next_of_kin";

    public function patients(){
    	return $this->hasMany('App\Patient');
    }
}
