<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
            	[
    	            'name' => 'Admin',
    	            'tel'   => '0705769370',
    	            'email' => 'admin@hospital.dev',
    	            'password' => bcrypt('admin'),
    	            'acl' => 'admin'
            	],
            	[
    	            'name' => 'Doctor',
    	            'tel'   => '0705769371',
    	            'email' => 'doctor@hospital.dev',
    	            'password' => bcrypt('doctor'),
    	            'acl' => 'doctor'
            	],
            	[
    	            'name' => 'Secretary',
    	            'tel'   => '0705769372',
    	            'email' => 'secretary@hospital.dev',
    	            'password' => bcrypt('secretary'),
    	            'acl' => 'secretary'
            	]
            ]
        );
    }
}
