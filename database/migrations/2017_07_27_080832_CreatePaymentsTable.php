<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id');
            $table->enum('mode', ['cash','cheque', 'insurance', 'credit'])->default('cash');
            $table->integer('amount')->default(0);
            $table->enum('status', ['paid', 'rejected', 'pending', 'overdue'])->default('pending');
            $table->timestamps();

            $table->foreign('patient_id')->references('id')->on('patients');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
