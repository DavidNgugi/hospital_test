<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('assigned_doctor_id')->nullable();
            $table->string('name');
            $table->date('dob');
            $table->string('insurance_cover');
            $table->integer('policy_number');
            $table->string('contacts');
            $table->enum('checked_in', ['true','false'])->default('false');
            $table->timestamps();

            $table->foreign('assigned_doctor_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patients');
    }
}
